![Gridiron Logo](http://chilehead.com/gridiron/images/gridiron.jpg)

## Doug Baron's Gridiron Recruits

### Goal 

> Create a single page application that will allow a user to select from football recruits based on various performace data.

#### Front-end

- Uses React.js.

- The page has a form that uses the criteria selection to search for top recruits and then display the results.

#### Information

- git repo with the [active code](https://bitbucket.org/dbaronster/gridiron-js)

- Link to the application [deployed on Heroku](https://nameless-badlands-68790.herokuapp.com/)
