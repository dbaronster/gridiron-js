var bodyParser = require('body-parser');
var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');
var PLAYERS_FILE = './players.json';
var options = {
	index: "index.html"
};

app.use(express.static(path.join(__dirname, '/public')));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use('/', express.static('app', options));

app.set('port', process.env.PORT || 3000);

app.get('/', function(req, res){
  res.send("got hello");
});

app.get('/api/players', function(req, res){
  var data = fs.readFileSync(PLAYERS_FILE);
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
});

app.post('/api/players', function(req, res){
  if(!req.body.Name || !req.body.pID){
    res.send("Error");
    return;
  }
  
  var data = JSON.parse(fs.readFileSync(PLAYERS_FILE));
  data.push(req.body);
  fs.writeFile(PLAYERS_FILE, JSON.stringify(data));
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
});

app.listen(app.get('port'), function(){
  console.log("Listening on port " + app.get('port'));
});
