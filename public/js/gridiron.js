/*  Gridiron JavaScript framework, version 1.1
 *    (c) 2007 Doug Baron
 *  Gridiron is freely distributable under the terms of an MIT-style license.
 *--------------------------------------------------------------------------*/

var gridiron = {};
"use strict";


gridiron.Player = {}

/*
	these are the attributes, the performance and meta data collected for each player
*/
gridiron.Player.keys = {
	FIRSTNAME : "firstName",
	LASTNAME : "lastName",
	GRADYEAR : "gradYear",
	HEADCOACH : "headCoach",
	HIGHSCHOOL : "highSchool",
	OFFPOSITION : "offPosition",
	DEFPOSITION : "defPosition",
	ZIPCODE : "zipCode",

	TIME10 : "time10",
	TIME20 : "time20",
	TIME40 : "time40",
	TIMESHUTTLE : "timeShttl",
	
	HEIGHT : "height",
	WEIGHT : "weight",
	SQUAT : "squat",
	SQUATREPS : "squatReps",
	BENCH : "bench",
	BENCHREPS : "benchReps",
	BROADJUMP : "broadJmp",
	VERTJUMP : "vertJmp"
};

// 'alias' keys to shorten collection literals
var _pk = gridiron.Player.keys;
gridiron.Player.keys.SPEED_KEYS = new Array (_pk.TIME10, _pk.TIME20, _pk.TIME40, _pk.TIMESHUTTLE );
gridiron.Player.keys.POWER_KEYS = new Array ( _pk.SQUAT, _pk.SQUATREPS, _pk.BENCH, _pk.BENCHREPS );
gridiron.Player.keys.EXPLOSIVE_KEYS = new Array (_pk.BROADJUMP, _pk.VERTJUMP );
gridiron.Player.keys.PLAYER_KEYS = new Array ( _pk.FIRSTNAME, _pk.LASTNAME, _pk.HEIGHT, _pk.WEIGHT );
gridiron.Player.keys.TEAM_KEYS = new Array ( _pk.GRADYEAR, _pk.HIGHSCHOOL, _pk.HEADCOACH, _pk.ZIPCODE );
gridiron.Player.keys.BODY_KEYS = new Array (_pk.HEIGHT, _pk.WEIGHT );
gridiron.Player.keys.POSITION_KEYS = new Array (_pk.OFFPOSITION, _pk.DEFPOSITION );


gridiron.Metric = function(key, value) {
	function member(arry, elem) {
		return arry.indexOf(elem) != -1;
	}
	this.key = key;
	this.isSpeed = member(gridiron.Player.keys.SPEED_KEYS, key);
	this.isNumeric = this.isSpeed 
		|| member(gridiron.Player.keys.POWER_KEYS, key) 
		|| member(gridiron.Player.keys.EXPLOSIVE_KEYS, key)
		|| member(gridiron.Player.keys.BODY_KEYS, key);
//	console.log("METRIC["+key+"] numeric? "+ this.isNumeric + "; speed? " + this.isSpeed + "; value:" + value);
	if (this.isNumeric)
		this.value = Number(value);
	else
		this.value = value.toLowerCase();
}

gridiron.Metric.prototype = {
	isNumeric: function() {
		return this.isNumeric();
	},
	compareValueTo: function(value) {
		if (this.isNumeric) {
			value = Number(value);
			var diff = value - this.value;
			if (this.isSpeed)
				diff = -diff;
			return diff;
		} else {
			value = value.toLowerCase();
			return (value == this.value)? 0 : (value > this.value? 1 : -1);
		}
	},
	test: function(value) { // test value for satisfying this metric
		if (this.isNumeric) {
			value = Number(value);
			var diff = value - this.value;
			if (this.isSpeed)
				diff = -diff;
			return diff >= 0;
		} else {
			value = value.toLowerCase();
			return value.startsWith(this.value);
		}
	},
	toString : function() {
		return "{" + this.key + ":" + this.value + '}';
	}
}

gridiron.Recruiter = function (players, criteria) {
	this.players = this.recruits = players;
	this.criteria = criteria;
	this.recruits = new Array();
	this.recalc();
}

gridiron.Recruiter.prototype = {
	getPlayers: function() {
		return this.players;
	},

	getRecruits: function() {
		return this.recruits;
	},
	
	getCriteria: function() {
		return this.criteria;
	},
	
	recalc: function() {
		this.recruits = this.filter(this.players, this.criteria);
		this.sort(this.recruits, this.criteria);
	},
	
	addCriterion: function(key, value) {
		var metric = this.getCriterion(key);
		if (metric) {
			metric.value = value;
		}
		else {
			this.criteria.push(new gridiron.Metric(key, value));
		}
		this.recalc();
	},
	
	getCriterion: function(key) {
		return this.criteria.find(function(metric) {
			return metric.key == key;
		});
	},
	
	deleteCriterion: function(key, value) {
		this.criteria.splice( this.criteria.indexOf(key), 1 );
		this.recalc();
	},
	
	setPlayers : function(players) {
		this.players = this.recruits = players;
		this.recalc();
	},
	
	reset: function() {
		this.criteria = new Array();
		this.recalc();
	},

	filter: function(candidates, criteria) {
		var recruits = new Array();
		
		for (var i = 0; i < candidates.length; i++) {
			var player = candidates[i];
			
			for (var j = 0; j < criteria.length; j++) {
				var need = criteria[j];
				var key = need.key;
				var got = player[key];
				if (got == null) {
					throw("missing key " + key);
				}
				if (!need.test(got)) { // filter it
					player = undefined;
					break;
				}
			}
			
			if (player)
				recruits.push(player);
		}
		
		return recruits;
	},
	
	
	/*
	 * sort the list of players according to the criteria provided
	 * the first criteria is the primary sort, the second the secondary, etc.
	 * @return inverse sorted list of the players input
	 */
	sort: function(recruits, criteria) {
		
		if (recruits == null)
			return;
		
		try {
			recruits.sort(function(m1, m2) {
				for (var i= 0; i < criteria.length; i++) {
					var key = criteria[i].key;
					var metric = new gridiron.Metric(key, m1[key]);
					var cmp = metric.compareValueTo(m2[key]);
					if (cmp != 0)
						return cmp;
				}
				return 0;});
		} catch (e) {
			alert("e?"+e);
		}
	}
}

gridiron.view = {

	getMetricGroupName: function (groupKey) {
		return gridiron.resources.metricGroup[groupKey.toLowerCase()];
	},
	
	getMetricName: function (metricKey) {
		return gridiron.resources.metric[metricKey];
	}
}

gridiron.resources = {
	metric: {
		time10: "10 Yards",
		time20: "20 Yards",
		time40: "40 Yards",
		timeShttl: "Pro Shuttle",
		squat: "Squat",
		squatReps: "Squat Reps",
		bench: "Bench",
		benchReps: "Bench Reps",
		broadJmp: "Broad Jump",
		vertJmp: "Vertical Jump",
		firstName: "First Name",
		lastName: "Last Name",
		height: "Height",
		weight: "Weight",
		gradYear: "Graduation Year",
		highSchool: "High School",
		headCoach: "Head Coach",
		zipCode: "Zip Code",
		offPosition: "Offense Position",
		defPosition: "Defense Position",
	},
	metricGroup : {
		power: "Power",
		speed: "Speed",
		explosive: "Explosiveness",
		player: "Player",
		team: "Team",
		position: "Position",
	}
}
