
"use strict";

var ShowCriteriaPrompt = React.createClass({
	render: function() {
		return (
			<div className="title">
				<p>No filters have been set.</p>
				<p><em>To establish a filter:</em></p><ol>
				<li>Click on a criterion to the right</li>
				<li>Enter a value for that metric</li>
				<li>Click the Add Criterion button</li></ol>
			</div>
		);
	}
});

var ShowMoreCriteriaPrompt = React.createClass({
	startOver : function() {
		this.props.onStartOver();
	},
	viewResults : function() {
		this.props.onViewResults();
	},
	render: function() {
		return (
			<div>
				<p><em>To add another filter, click a criterion to the right</em></p>
				<p>
					<button type="button" onClick={this.startOver}>Start Over</button>
					<button type="button" onClick={this.viewResults}>View Results</button>
				</p>
			</div>
		);
	}
});

var ShowMatchSummary = React.createClass({
	render: function() {
		var matched = this.props.recruits.length;
		var max = 10;
		var matches;
		if (matched > 0) {
			//$("viewResultsButton").show();
			matches = ""+matched;
		} else {
			//$("viewResultsButton").hide();
			matches = "No";
		}
		var bridge = (matched == 1)?'player meets' : 'players meet';
		return (
			<p><b>{matches}</b> {bridge} these criteria.</p>
		);
	}
});

var ShowCriteriaList = React.createClass({
	render: function() {
		var criteriaNodes = this.props.criteria.map(function(metric) {
			return (
				<Criteria key={metric.key} metric={metric} />
			);
		});
		return (
			<div className="criteriaList">
				Filtering on:
				<ol>
					{criteriaNodes}
				</ol>
				<ShowMatchSummary recruits={this.props.recruits}/>
			</div>
		);
	}
});

var getMetricDisplayProperties = function(metricKey) {
	var key = metricKey;
	var metric = gridiron.resources.metric[key];
	var rel = "begins with";
	var units = "";

	if (/time/.test(key)) {
		rel = "<=";
		units = "seconds";
	}
	if (/weigh/.test(key) || "squat" == key || "bench" == key) {
		rel = ">=";
		units = "pounds";
	}
	if (/Jmp/.test(key) || /heigh/.test(key)) {
		rel = ">=";
		units = "inches";
	}
	if (/Pos/.test(key) || /ear/.test(key) || /zip/.test(key)) {
		rel = "is";
	}
	if (/Reps/.test(key)) {
		rel = ">=";
	}
	return {label:metric, relation:rel, units:units};
}

var getMetricValueRange = function(recruiter, metricKey) {
	// sort selected players by this criterion - or all players if none are selected
	var metric = new gridiron.Metric(metricKey, "");
	// sort selected players by this criterion - or all players if none are selected
	var selectedPlayers = recruiter.recruits;
	if (selectedPlayers.length == 0) {
		selectedPlayers = recruiter.players;
	}

	recruiter.sort(selectedPlayers, [metric]);
	var sortedRecruits = selectedPlayers;
	
	// grab first and last for range
	var firstPlayer = sortedRecruits[0];
	var lastPlayer = sortedRecruits[sortedRecruits.length-1];
	var firstPlayerMetricValue = firstPlayer[metricKey];
	var lastPlayerMetricValue = lastPlayer[metricKey];
	// show range in logical order for metric
	if (metric.isSpeed) {
		return {min : firstPlayerMetricValue,
				max : lastPlayerMetricValue};
	}
	else {
		return {min : lastPlayerMetricValue,
				max : firstPlayerMetricValue};
	}
}

var Criteria = React.createClass({
	render: function() {
		var keyLabel = gridiron.view.getMetricName(this.props.metric.key);
		var displayProps = getMetricDisplayProperties(this.props.metric.key);
		return (
			<li className="criteria">
				{displayProps.label} {displayProps.relation} {this.props.metric.value} {displayProps.units}
			</li>
		);
	}
});

var MetricChoice = React.createClass({
	pickCriterion: function(e) {
		var key = e.target.value;
	    this.props.onMetricClick(key);
	},

    render: function() {
    	var metricKey = this.props.metricKey;
		return (<div>
			<input type="radio" name="criterion" value={this.props.metricKey} onClick={this.pickCriterion} id="{this.props.metricKey}Metric"/>
			<span>{gridiron.view.getMetricName(metricKey)}</span>
			</div>
    )}
});

var MetricGroup = React.createClass({
    render: function() {
    	var groupKey = this.props.groupKey;
    	var metricKeys = this.props.data[groupKey];
    	var onMetricClick = this.props.onMetricClick;
        var metricNodes = metricKeys.map(function(metric) {
            return (
            	<MetricChoice onMetricClick={onMetricClick} key={metric} metricKey={metric} />
            );
    });
    return (
      <div className="commentList">
        <div className="metric-group">
             <p className="title">{gridiron.view.getMetricGroupName(groupKey)}</p>
             <div className="select">
                {metricNodes}
             </div>
        </div>
      </div>
    )}
});

var MetricGrid = React.createClass({
	getInitialState: function() {
		var metrics = {
				FIRSTNAME : "firstName",
				LASTNAME : "lastName",
				GRADYEAR : "gradYear",
				HEADCOACH : "headCoach",
				HIGHSCHOOL : "highSchool",
				OFFPOSITION : "offPosition",
				DEFPOSITION : "defPosition",
				ZIPCODE : "zipCode",

				TIME10 : "time10",
				TIME20 : "time20",
				TIME40 : "time40",
				TIMESHUTTLE : "timeShttl",
				
				HEIGHT : "height",
				WEIGHT : "weight",
				SQUAT : "squat",
				SQUATREPS : "squatReps",
				BENCH : "bench",
				BENCHREPS : "benchReps",
				BROADJUMP : "broadJmp",
				VERTJUMP : "vertJmp"
			};

		return {data: {
			SPEED: [metrics.TIME10, metrics.TIME20, metrics.TIME40, metrics.TIMESHUTTLE ],
			POWER: [metrics.SQUAT, metrics.SQUATREPS, metrics.BENCH, metrics.BENCHREPS ],
			EXPLOSIVE: 	[metrics.BROADJUMP, metrics.VERTJUMP ],
			PLAYER: [metrics.FIRSTNAME, metrics.LASTNAME, metrics.HEIGHT, metrics.WEIGHT ],
			TEAM: [metrics.GRADYEAR, metrics.HIGHSCHOOL, metrics.HEADCOACH, metrics.ZIPCODE ],
			POSITION: [metrics.OFFPOSITION, metrics.DEFPOSITION ]}}
	},
	
	render: function() {
    	return (
    	<table cellSpacing="0" cellPadding="0" border="0" style={{verticalAlign: top}}><tbody>
    		<tr>
    			<td><MetricGroup onMetricClick={this.props.onMetricClick} data={this.state.data} groupKey="SPEED"/> </td>
    			<td><MetricGroup onMetricClick={this.props.onMetricClick} data={this.state.data} groupKey="PLAYER"/> </td>
    		</tr>
    		<tr>
    			<td><MetricGroup onMetricClick={this.props.onMetricClick} data={this.state.data} groupKey="POWER"/> </td>
    			<td><MetricGroup onMetricClick={this.props.onMetricClick} data={this.state.data} groupKey="TEAM"/> </td>
    		</tr>
    		<tr>
    			<td><MetricGroup onMetricClick={this.props.onMetricClick} data={this.state.data} groupKey="EXPLOSIVE"/> </td>
    			<td><MetricGroup onMetricClick={this.props.onMetricClick} data={this.state.data} groupKey="POSITION"/> </td>
    		</tr>
    	</tbody></table>
    );}
});

var CriteriaBox = React.createClass({
	render: function() {
		var criteria = this.props.recruiter.getCriteria();
		var recruits = this.props.recruiter.getRecruits();
		if (criteria.length > 0) {
			return (
				<ShowCriteriaList criteria={criteria} recruits={recruits}/>
			);
		}
		else {
			return (
				<ShowCriteriaPrompt />
			);
		}
	}
});

var Player = React.createClass({
	render: function() {
		return (
			<div className="player">
				{this.props.player.firstName} {this.props.player.lastName}
			</div>
		);
	}
});

var PlayerList = React.createClass({
	render: function() {
		var playerNodes = this.props.data.map(function(player) {
			return (
				<Player key={player.id} player={player}>
				</Player>
			);
		});
		return (
			<div className="playerList">
				{playerNodes}
			</div>
		);
	}
});

var MetricEditor = React.createClass({
	getInitialState: function() {
		return {metricKey: '', metricValue: ''};
	},
	cancelCriterion : function() {
		this.props.onCancel();
	},
	acceptCriterion : function() {
		this.props.onSubmit(this.state.metricValue);
		this.setState({metricKey: '', metricValue : ''});
	},
	handleStartOver : function() {
		this.props.onStartOver();
	},
	handleViewResults : function() {
		this.props.onViewResults();
	},
	handleValueChange: function(e) {
		this.setState({metricValue : e.target.value});
	},
	handleDone: function(e) {
		document.location='down.html';
	},
	handleLoad: function(e) {
		$("#metricValue").select();
	},

	render: function() {
		if (this.props.metricInEdit) {
			var key = this.props.metricInEdit;
			var len = 5;
			var recruiter = this.props.recruiter;
			var displayProps = getMetricDisplayProperties(key);
			var metricRange = getMetricValueRange(recruiter, key);

			//if there's already criteria for this metric, use that value
			if (key != this.state.metricKey) {
				this.state.metricKey = key;
				var metric = recruiter.getCriterion(key);
				this.state.metricValue = (metric && metric.value) || '';
				window.setTimeout(this.handleLoad, 100);
			}

			return (
				<div id="metricEdit">
				<div id="metricEditFields">
					{displayProps.label} <span> {displayProps.relation} </span> <input id="metricValue" type="text" onChange={this.handleValueChange} size={len} value={this.state.metricValue} /> {displayProps.units}
				</div>
				<div id="metricRange">Range: <span id="metricMin">{metricRange.min}</span> to <span id="metricMax">{metricRange.max}</span></div>
					<button type="button" onClick={this.acceptCriterion}>Add Criterion</button>
					<button type="button" onClick={this.cancelCriterion}>Cancel</button>
				</div>
		)}
		else {
			if (this.props.recruiter.getCriteria().length == 0) {
				return (
					<button type="button" onClick={this.handleDone}>Done</button>
				);
			}
			else {
				return <ShowMoreCriteriaPrompt onStartOver={this.handleStartOver} onViewResults={this.handleViewResults}/>
			}
		}
	}
});

var PlayerDetail = React.createClass({
	render: function() {
		var player = this.props.player;
		var hgt = Math.round(player["height"]);
		var hgtFtIn = Math.round(hgt/12) + "' " + (hgt%12) + '"';
		var fullName = player["firstName"] + ' ' + player["lastName"];
		var squat = player["squat"] + ' / ' + player["squatReps"];
		var bench = player["bench"] + ' / ' + player["benchReps"];
		return (
			<div>
			<div className="break" style={{lineHeight:"1px"}}><br/></div>
			<div className="player"><p>
			<span className="pid">{player["id"]}</span>
			<span className="player">{fullName}</span>
			<span className="name">{player["highSchool"]}</span>
			<span className="year">{player["gradYear"]}</span>
			<span className="name">{player["headCoach"]}</span>
			<span className="zip">{player["zipCode"]}</span>
			<span className="height">{hgtFtIn}</span>
			<span className="weight">{player["weight"]}</span>
			<span className="pos">{player["offPosition"]}</span>
			<span className="pos">{player["defPosition"]}</span>
			<span className="time">{player["time10"]}</span>
			<span className="time">{player["time20"]}</span>
			<span className="time">{player["time40"]}</span>
			<span className="time">{player["timeShttl"]}</span>
			<span className="power">{squat}</span>
			<span className="power">{bench}</span>
			<span className="expl">{player["broadJmp"]}</span>
			<span className="expl">{player["vertJmp"]}</span>
			</p></div>
			</div>
		);
	}
});

var ShowResults = React.createClass({
	handleBack : function() {
		this.props.onBack();
	},
	handlePrint : function() {
		window.print();
	},
	//TODO: add key parameter, bold css class to bolden criteria columns
	calcClass : function (key) {
		return key + ' bold';
	},

	render: function() {
		var recruits = this.props.recruiter.getRecruits();
		var matched = recruits.length;
		var playerNodes = recruits.map(function(player) {
			return (
				<PlayerDetail key={player.id} player={player} />
			);
		});
		return (
				<div id="printView">
				<img src="images/gridiron.jpg" alt="Gridiron Recruits" border="1"/>
					<div className="players">
						<p className="header">
							<span className="pid">#</span>
							<span className={this.calcClass("player")}>&nbsp;Name</span>
							<span className="name">High School</span>
							<span className="year">Grad Year</span>
							<span className="name">Head Coach</span>
							<span className="zip">Zip</span>
							<span className="height">Height</span>
							<span className="weight">Weight</span>
							<span className="pos">Offense Position</span>
							<span className="pos">Defense Position</span>
							<span className="time">10 Yards</span>
							<span className="time">20 Yards</span>
							<span className="time">40 Yards</span>
							<span className="time">Pro Shttle</span>
							<span className="power">Squat / Reps</span>
							<span className="power">Bench / Reps</span>
							<span className="expl">Broad Jump</span>
							<span className="expl">Vertical Jump</span>
						</p>
					<div className="playerList">
						{playerNodes}
					</div>
					<div className="tightForm">
						<form>
							<div id="printing">
								<button type="button" onClick={this.handleBack}>Back</button>
								<button type="button" onClick={this.handlePrint}>Print</button>
								<div style={{fontSize:"large"}}>&nbsp;&nbsp; <span>For best results, Print in Landscape orientation.</span></div>
							</div>
						</form>
					</div>
				</div>
			</div>

		);
	}
});

var App = React.createClass({
	loadPlayersFromServer: function() {
		 $.ajax({
			url: this.props.url,
			dataType: 'json',
			cache: false,
			success: function(data) {
				var players = data;
				var criteria = this.state.recruiter.getCriteria();
				this.setState({recruiter: new gridiron.Recruiter(players, criteria),
					metricInEdit : undefined//"time20"
				});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});
	},
	getInitialState: function() {
		var players = [];
		var criteria = [];
        return {
			recruiter : new gridiron.Recruiter(players, criteria),
			metricInEdit : undefined
        }
    },
    componentDidMount : function() {
    	this.loadPlayersFromServer();
    	//setInterval(this.loadMetricsFromServer, this.props.pollInterval);
    },
    handleMetricClick : function(metricKey) {
    	this.setState({metricInEdit : metricKey});
    },
    handleMetricSubmit : function(metricValue) {
    	this.state.recruiter.addCriterion(this.state.metricInEdit, metricValue);
    	this.setState({
    		criteria : this.state.recruiter.getCritria,
			metricInEdit : undefined
    		})
    },
    handleMetricCancel : function() {
    	this.setState({metricInEdit : undefined});
	},
	handleStartOver : function() {
		this.state.recruiter.reset();
    	this.setState({criteria : []})
	},
	handleViewResults : function() {
    	this.setState({view : "showResults"})
	},
	handleBackToRecruiter : function() {
    	this.setState({view : "showRecruiter"})
	},

	render : function() {
		//<PlayerBox players={this.state.recruiter.getRecruits()} url="/api/players" />,
		if (this.state.view == "showResults") {
			return <ShowResults 
				recruiter={this.state.recruiter}
				onBack={this.handleBackToRecruiter}
				/>
		}
		else {
			return (
				<div>
				<h1>Gridiron Recruits - Select Criterion</h1>
				<table cellSpacing="0" cellPadding="0" border="0"><tbody>
					<tr>
						<td valign="top">
							<div className="left-side">
							<div className="metric-results">
								<div id="haveCriteria">
									<CriteriaBox recruiter={this.state.recruiter} />
								</div>
								<MetricEditor recruiter={this.state.recruiter} metricInEdit={this.state.metricInEdit} 
									recruiter={this.state.recruiter}
									onSubmit={this.handleMetricSubmit}
									onCancel={this.handleMetricCancel}
									onStartOver={this.handleStartOver}
									onViewResults={this.handleViewResults}
								/>
							</div>
							</div>
						</td>
						<td valign="top">
							<div id="metricGrid" className="right-side">
								<MetricGrid onMetricClick={this.handleMetricClick} />
							</div>
						</td>
					</tr>
				</tbody></table>
				</div>
			)
		}
	}
});

ReactDOM.render(
	  <App url="/api/players"/>, document.getElementById('filterView')
);
